var contentfulClient = contentful.createClient({
    accessToken: 'AGmYdrnLI6CRKYTEJ3F30_xJUEbabhEQiEQxaAZ8jjM',
    space: 'dy7vrim3h7y0'
  })

    // Fc Home
    let fcHero = document.getElementById('hero');
    let futureOfAgric = document.getElementById('agric_future');
    let fcProductsHeader = document.getElementById('fc_products_header');
    let fcProducts = document.getElementById('fc_products');
    let testimonialId = document.getElementById('contentful_testimonial');
    if(fcHero){
        contentfulClient.getEntry('6iy22b10Ehf8Z2rzdWkjs1')
        .then(entry => {
            // Hero section 
            let heroTitle = entry.fields.heroTitle;
            fcHero.innerHTML += heroTitle;

            //future of agriculture section
            let agricFutureTitle = entry.fields.futureOfAgricultureSection.fields.title;
            let agricFutureDescription = entry.fields.futureOfAgricultureSection.fields.description;
            let futureImgAlt = entry.fields.futureOfAgricultureSection.fields.image.fields.title;
            let futureImgUrl = entry.fields.futureOfAgricultureSection.fields.image.fields.file.url
            const agricFutureText = `   <div class="text_details">
                                            <h4>${agricFutureTitle}</h4>
                                            <p>${agricFutureDescription}</p>
                                        </div>
                                        <div class="img_container">
                                            <img src="${futureImgUrl}" alt="${futureImgAlt}">
                                        </div>`;
            futureOfAgric.innerHTML +=  agricFutureText;

            //product section 
            let productHeader = entry.fields.productSectionTitle;
            fcProductsHeader.innerHTML += productHeader;
            // product loop 
            let mapProduct = entry.fields.products.map((data)=>(
                `<a href="#" class="product_">
                    <div class="img_container">
                        <img src="${data.fields.productImage.fields.file.url}" alt="${data.fields.productImage.fields.title}">
                    </div>
                    <div class="text_detail">
                        <h5>${data.fields.name}</h5>
                        <p>${data.fields.shortDescription}</p>
                    </div>
                </a>`
            )).join('')
            fcProducts.innerHTML +=  mapProduct; 

            //Testimonial
            let testimonials = entry.fields.testimonials.map((data)=>(
                `   <div class="carousel_item">
                        <img src="${data.fields.image.fields.file.url}" class="user" alt="${data.fields.image.fields.title}">
                        <div class="content">
                            <svg width="41" height="39" viewBox="0 0 41 39" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="41" height="39">
                                <rect width="41" height="39" fill="#C4C4C4"/>
                                </mask>
                                <g mask="url(#mask0)">
                                <path d="M18.104 27.232C18.104 29.088 17.4 30.72 15.992 32.128C14.648 33.536 12.856 34.24 10.616 34.24C8.056 34.24 6.008 33.344 4.472 31.552C3 29.76 2.264 27.584 2.264 25.024C2.264 21.824 2.744 19.136 3.704 16.96C4.728 14.72 5.976 12.928 7.448 11.584C8.92 10.176 10.52 9.152 12.248 8.512C13.976 7.808 15.608 7.392 17.144 7.264V13.12C15.288 13.44 13.528 14.304 11.864 15.712C10.264 17.056 9.4 18.848 9.272 21.088C9.784 20.768 10.456 20.608 11.288 20.608C13.464 20.608 15.128 21.216 16.28 22.432C17.496 23.584 18.104 25.184 18.104 27.232ZM37.016 27.232C37.016 29.088 36.312 30.72 34.904 32.128C33.56 33.536 31.768 34.24 29.528 34.24C26.968 34.24 24.92 33.344 23.384 31.552C21.912 29.76 21.176 27.584 21.176 25.024C21.176 21.824 21.656 19.136 22.616 16.96C23.64 14.72 24.888 12.928 26.36 11.584C27.832 10.176 29.432 9.152 31.16 8.512C32.888 7.808 34.52 7.392 36.056 7.264V13.12C34.2 13.44 32.44 14.304 30.776 15.712C29.176 17.056 28.312 18.848 28.184 21.088C28.696 20.768 29.368 20.608 30.2 20.608C32.376 20.608 34.04 21.216 35.192 22.432C36.408 23.584 37.016 25.184 37.016 27.232Z" fill="#ABBA16"/>
                                </g>
                            </svg>
                                
                            <p class="testimony_">${data.fields.testimony}</p>
                            <p class="t_footer">- ${data.fields.name}. ${data.fields.jobRoleAndLocation}</p>
                            <img src="img/farmer.png" class="mobile_img" alt="farmer">
                        
                        </div>
                    </div>`
            )).join('');
                testimonialId.innerHTML += testimonials;
                if(testimonialId.innerHTML){
                    function delayCarousel(){
                        $('.testimonial_carousel_container').owlCarousel({
                            stagePadding: 35,
                            loop: true,
                            items: 1,
                            margin: 45,
                            dots: true,
                            autoplay: true,
                            autoplayTimeout: 5000,
                            autoplayHoverPause: true,
                            responsiveClass: true,
                            responsive: {
                                0: {
                                items: 1,
                                nav: false,
                                stagePadding: 15,
                                margin: 25,
                                },
                                375: {
                                items: 1,
                                nav: false,
                                margin: 45,
                                stagePadding: 35
                
                                },
                                991: {
                                items: 2,
                                nav: false,
                                margin: 25,
                                },
                                1200: {
                                stagePadding: 35,
                                items: 1,
                                margin: 45,
                        
                                }
                            }
                          })
                      }
                          // use setTimeout() to execute
                          setTimeout(delayCarousel, 100)
                }
            // console.log(entry.fields)            
          })
      }